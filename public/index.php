<?php

require __DIR__.'/../config/app.php';

if (config_erede('env') == 'local') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

require __DIR__ . '/../src/helper.php';

require __DIR__.'/../vendor/autoload.php';

require __DIR__ . '/../routes/fast-route.php';



