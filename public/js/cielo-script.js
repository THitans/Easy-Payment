var card = new Card({
    // a selector or DOM element for the form where users will
    // be entering their information
    form: 'form', // *required*
    // a selector or DOM element for the container
    // where you want the card to appear
    container: '.cartaodaora', // *required*

    formSelectors: {
        numberInput: 'input[name="cardNumber"]', // optional — default input[name="number"]
        nameInput: 'input[name="cardholderName"]', // optional - defaults input[name="name"]
        expiryInput: 'input[name="expirationDate"]',
        // yearInput: 'input[name="expirationYear"]',
        // expiryInput: 'input[name="card_date"]', // optional — default input[name="expiry"]
        cvcInput: 'input[name="securityCode"]', // optional — default input[name="cvc"]
    },

    width: 440, // optional — default 350px
    formatting: true, // optional - default true

    // Strings for translation - optional
    messages: {
        validDate: 'valid\ndate', // optional - default 'valid\nthru ndate'
        monthYear: 'mm/yy', // optional - default 'month/year'
    },
    // Default placeholders for rendered fields - optional
    placeholders: {
        number: '•••• •••• •••• ••••',
        name: 'Nome Completo',
        expiry: '••/••',
        cvc: '•••'
    },
    debug: true // optional - default false
});


$(function () {
    $('#nome_cartao').focusout(function () {
        // Uppercase-ize contents
        this.value = this.value.toLocaleUpperCase();
    });
});