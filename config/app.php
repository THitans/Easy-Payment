<?php

if (! function_exists('get_env')) {

    function get_env()
    {
        return 'local';
    }
}

if (! function_exists('route_base')) {

    function route_base()
    {
        return '/public';
    }
}

if (! function_exists('config_erede')) {

    function config_erede($index)
    {
        $data = [
            'env' => get_env(),
            'filiation' => '10001045',
            'token' => 'ba8cab98bd424dd1991a651393fc33e6',
            'route_base' => route_base()
        ];

        $data['erede_url'] = get_env() == 'local' ?  'https://api.userede.com.br/desenvolvedores/v1/' : 'https://api.userede.com.br/erede/v1/';

        return $data[$index];
    }
}

if (! function_exists('config_cielo')) {

    function config_cielo($index)
    {
        $data = [
            'env' => get_env(),
//            'merchant_id' => 'fb4ce34e-70cf-4c39-a52d-7971e43171ea',
//            'merchant_key' => 'WPHJKRFYBGQENTRYUZXGAAUUEGAFFAPXKLBFBCDE',
            'merchant_id' => 'd9e8f3dc-ab2f-45c3-b690-0ca7586f73e0',
            'merchant_key' => 'TXWXZHENWTUOTVEUSMNJHNJPSGCFRNOCQXYSXYPK',
            'route_base' => route_base()
        ];

        // Somente para requisições
        $data['erede_url'] = get_env() == 'local' ?  'https://apisandbox.cieloecommerce.cielo.com.br' : 'https://api.cieloecommerce.cielo.com.br/';

        return $data[$index];
    }
}