<?php
/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:31
 */

namespace ERede\Entity;

/**
 * Class Authorization
 * Docs para atributos https://www.userede.com.br/desenvolvedores/pt/produto/e-Rede#documentacao
 * @package ERede\Entity
 */
class Authorization
{
    use Model;

    /**
     * Define se a transação terá captura automática ou posterior.
     * O não envio desse campo será considerado a captura automática (true)
     *
     * @var bool
     */
    public $capture = true;

    /**
     * Código do pedido gerado pelo estabelecimento
     *
     * @var string
     */
    public $reference;

    /**
     * Valor total da transação sem separador de milhar e decimal.
     * Exemplos: R$ 10,00 = 1000 | R$ 0,50 = 50
     *
     * @var integer
     */
    public $amount;

    /**
     * Nome do portador impresso no cartão.
     *
     * @var integer
     */
    public $cardholderName;

    public $cardNumber;

    /**
     * Mês de vencimento do cartão. De 1 a 12.
     *
     * @var integer
     */
    public $expirationMonth;

    /**
     * Ano de vencimento do cartão
     * Ex.: 2021 ou 21
     * @var integer
     */
    public $expirationYear;

    public $securityCode;

    /**
     * Tipo de transação a ser realizada.
     * - Para transações de crédito, utilize credit
     * - Para transações de débito, utilize debit
     * O não envio desse campo será considerado credit
     *
     * @var string
     */
    public $kind;

    /**
     * @var string Frase personalizada que será impressa na fatura do portador
     */
    public $softDescription;

    /**
     * Json para usar no POST para API
     * @return string json
     */
    public function getDataBodyRequest()
    {
        return [
            'capture' => $this->capture,
            'reference' => $this->reference,
            'amount' => $this->amount,
            'cardHolderName' => $this->cardholderName,
            'cardNumber' => $this->cardNumber,
            'expirationMonth' => $this->expirationMonth,
            'expirationYear' => $this->expirationYear,
            'securityCode' => $this->securityCode,
            'kind' => $this->kind,
            'softDescription' => $this->softDescription,
        ];
    }
}