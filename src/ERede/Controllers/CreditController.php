<?php

/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:05
 */

namespace ERede\Controllers;

use ERede\Entity\Authorization;
use GuzzleHttp\Exception\GuzzleException;

class CreditController
{
    use AuthorizeTrait;

    // https://lipis.github.io/bootstrap-sweetalert/
    public function payment()
    {
        $routeAssets = config_erede('route_base');

        include view_path('erede/credit/payment.php');
    }

    public function authorizeCredit()
    {
        try {
            $randNumber = time() . rand(1, 1000);

            $authorizationEntity = new Authorization();
            $authorizationEntity->fill($_POST);
            $authorizationEntity->reference = 'P' . $randNumber;
            $authorizationEntity->kind = 'credit';
            $authorizationEntity->softDescription = 'Inst.Vid.Ren';

            $response = $this->authorize($authorizationEntity->getDataBodyRequest());

            echo json_response(200, \GuzzleHttp\json_decode($response->getBody()));
        } catch (GuzzleException $guzzleException) {
            echo json_response(500, \GuzzleHttp\json_decode($guzzleException->getResponse()->getBody(true)));
        } catch (\Exception $exception) {
            echo json_response(500, $exception->getMessage());
        }

    }

    public function authorizeDebit()
    {
        try {
            $randNumber = time() . rand(1, 1000);

            $authorizationEntity = new Authorization();
            $authorizationEntity->fill($_POST);
            $authorizationEntity->reference = 'P' . $randNumber;
            $authorizationEntity->kind = 'debit';
            $authorizationEntity->softDescription = 'Inst.Vid.Ren';

            $response = $this->authorize($authorizationEntity->getDataBodyRequest());

            echo json_response(200, \GuzzleHttp\json_decode($response->getBody()));

        } catch (GuzzleException $guzzleException) {
            echo json_response(500, \GuzzleHttp\json_decode($guzzleException->getResponse()->getBody(true)));
        } catch (\Exception $exception) {
            echo json_response(500, $exception->getMessage());
        }
    }
}