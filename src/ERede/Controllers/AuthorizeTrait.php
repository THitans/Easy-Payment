<?php

/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:06
 */

namespace ERede\Controllers;

use GuzzleHttp\Exception\GuzzleException;

trait AuthorizeTrait
{
    private function authorize($body)
    {
        return $this->client()
            ->request('POST', config_erede('erede_url') . 'transactions', [
                'headers' => [
                    'Authorization' => $this->getAuthorizationHeader(),
                    'Content-Type' => 'application/json'
                ],
                'json' => $body
            ]);
    }

    /**
     * Set Authorization Header
     */
    private function getAuthorizationHeader()
    {
        return 'Basic ' . base64_encode(config_erede('filiation') . ':' . config_erede('token'));
    }

    /**
     * @return \GuzzleHttp\Client
     */
    private function client()
    {
        return new \GuzzleHttp\Client();
    }
}