<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Instituto Vida Renovada - Rede API</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="<?= $routeAssets . '/css/card.css' ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?= $routeAssets . '/css/sweetalert.css' ?>">
    <style>
        .loader {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 50%;
            z-index: 2;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: rgba(0, 0, 0, 0.4);
        }

        .loader::before {
            height: 1em;
            width: 1em;
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -0.5em;
            margin-top: -1em;
            content: '';
            animation: spin 1s ease-in-out infinite;
            background: url('img/loader.svg') center center;
            background-size: cover;
            line-height: 1;
            text-align: center;
            font-size: 2em;
            color: rgba(#000, 0.75);
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>


<body>

<div class="container" style="padding-top: 10px;">

    <div id="google_translate_element" style="position:fixed; top:0; z-index:999;"></div>

    <div class="row">
        <div class="col-sm-6 col-md-2">
            <div class="partner">
                <a href="#">
                    <img src="<?= $routeAssets . '/img/logo-rede.png' ?>" style="width: 100%; height: 83px; object-fit: cover;">
                </a>
            </div>
        </div>

        <div class="col-md-8 offset-md-1 mt-3">
            <h3 style="padding-top: 10px;">Instituto Vida Renovada - CHECKOUT</h3>
        </div>

    </div>

    <hr>

    <div class="row">
        <form method="post" action="/">

            <div class="row">

                <div class="col-md-4 offset-md-4 text-center">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Valor da sua doação</label>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <select name="currency" class="input-group-text" id="inputGroupSelect01">
                                    <option value="BRL" selected>R$</option>
<!--                                    <option value="USD">US$</option>-->
<!--                                    <option value="EUR">€</option>-->
                                </select>
                            </div>
                            <input type="text" name="amount" class="form-control money" aria-label="Valor doação"
                                   placeholder="0,00" autofocus required>
                        </div>

                    </div>

                </div>

            </div>


            <h3 class="mb-2">Informações do pagamento</h3>

            <div class="row">

                <div class="col-md-3 mb-4">
                    <div class="form-check form-check-inline" style="width: 50%;">
                        <input class="option-input card-type"
                               type="radio"
                               name="card_type"
                               id="inlineRadio2"
                               value="authorizeCredit"
                               checked required>
                        <label class="form-check-label" for="inlineRadio2">Crédito </label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="option-input card-type"
                               type="radio"
                               name="card_type"
                               id="inlineRadio1"
                               value="authorizeDebit"
                               required>
                        <label class="form-check-label" for="inlineRadio1">Débito</label>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6"
                     style="background-color: #f8f8f8; border: 1px solid #ccc; padding: 45px 30px 0 30px; border-radius: 10px; box-shadow: 0 0 10px #f4f4f2;">
                    <div class="row">

                        <div class="col-md-6">
                            <label for="cardNumber">Número do cartão</label>
                            <div class="input-group form-group">
                                <input type="text" name="cardNumber" class="form-control credit-card"
                                       id="cardNumber"
                                       required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                      <i class="fa fa-credit-card"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cardholderName">Nome impresso no cartão</label>
                                <input type="text"
                                       name="cardholderName"
                                       class="form-control"
                                       id="cardholderName"
                                       required>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <p style="display: block; margin-bottom: .5rem;">Válido até</p>
                            <div class="d-flex">
                                <label for="expirationMonth">
                                    <span class="payment-card__hint"></span>
                                    <input type="text" name="expirationMonth"
                                           class="form-control month_input payment-card__field" id="expirationMonth"
                                           placeholder="MM"
                                           required>
                                </label>

                                <span class="payment-card__separator">/</span>

                                <label for="expirationYear">
                                    <span class="payment-card__hint"></span>
                                    <input type="text" name="expirationYear"
                                           class="form-control year_input payment-card__field" id="expirationYear"
                                           placeholder="YY"
                                           required>
                                </label>

                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="securityCode">Código de segurança</label>
                                <input type="text" name="securityCode" class="form-control ccv"
                                       id="securityCode"
                                       placeholder="CCV" data-minlength="3" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="cartaodaora"></div>
                </div>

            </div>

            <div class="col-md-4 text-center mt-4 mb-5">
                <button name="send-donation" class="btn btn-success pull-right btn-payment">Seguir para pagamento
                </button>
            </div>

        </form>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="<?= $routeAssets . '/js/jquery.card.js' ?>"></script>

    <script src="<?= $routeAssets . '/js/erede-script.js' ?>"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="<?= $routeAssets . '/js/sweetalert.min.js' ?>"></script>


    <script>
        $(document).ready(function () {
            $('.money').mask("##0,00", {
                reverse: true
            });

            $('.credit-card').mask("0000 0000 0000 0000");

            $('.ccv').mask("0000");

            $('.month_input').mask("00");

            $('.year_input').mask("00");

            $route = '<?= $routeAssets . '/erede/' ?>' + $('input[name=card_type]:checked').val();
            $('.card-type').on('change', function(){
                $route = '<?= $routeAssets . '/erede/' ?>' + $(this).val();
            });

            $('.btn-payment').on('click', function () {
                var $emptyFields = $('form input').filter(function () {
                    return $.trim(this.value) === "";
                });

                if (!$emptyFields.length) {
                    var $this = $(this);
                    var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Processando...';
                    $this.attr('disabled', true);
                    if ($(this).html() !== loadingText) {
                        $this.data('original-text', $(this).html());
                        $this.html(loadingText);
                    }

                    try {
                        data = getInputToPost();

                        $.post($route, data, function () {
                            successMessage();

                            $this.html($this.data('original-text'));
                        })
                            .fail(function (err) {
                                var response = err.responseJSON;
                                // console.log(response);
                                var message = translateRedeItauMessage(response.message.returnCode);

                                customErrorMessage(message);
                            })
                    } catch (err) {
                        console.log(err);
                        errorMessage();
                    }
                    return false;
                }

            });

            function getInputToPost() {
                return {
                    "amount": $('input[name=amount]').val(),
                    "cardholderName": $('input[name=cardholderName]').val(),
                    "cardNumber": $('input[name=cardNumber]').val(),
                    "expirationMonth": $('input[name=expirationMonth]').val(),
                    "expirationYear": $('input[name=expirationYear]').val(),
                    "securityCode": $('input[name=securityCode]').val()
                };
            }

            function errorMessage() {
                swal({
                        title: "Ocorreu um Erro!",
                        text: "Tente novamente ou entre em Contato com o Administrador do Sistema!",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Tentar Novamente!",
                        closeOnConfirm: true
                    },
                    function () {
                        location.reload();
                    });
            };

            function successMessage() {
                swal({
                        title: "Obrigado!",
                        text: "Recebemos sua Contribuição!",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "OK!",
                        closeOnConfirm: true
                    },
                    function () {
                        location.reload();
                    });
            };

            function customErrorMessage(text) {
                swal({
                        title: "Ocorreu um Erro!",
                        text: text,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Tentar Novamente!",
                        closeOnConfirm: true
                    },
                    function () {
                        location.reload();
                    });
            };

            function translateRedeItauMessage(index) {
                var messages = {
                    1: 'Ano de Validade: Verifique o tamanho do texto.',
                    2: 'Ano de Validade: Formato Inválido',
                    3: 'Ano de Validade: É obrigatório.',
                    15: 'Código de Segurança: Verifique o tamanho do texto.',
                    16: 'Código de Segurança: Formato Inválido',
                    35: 'Mês de Validade: É obrigatório.',
                    36: 'Número do Cartão: Verifique o tamanho do texto.',
                    37: 'Número do Cartão: Formato Inválido',
                    38: 'Número do Cartão: É obrigatório.',
                    53: 'Transação não Permitida. Entre em Contato com a REDE.',
                    55: 'Nome do Titular: Verifique o tamanho do texto.',
                    56: 'Erro de dados Reportado. Tente novamente.',
                    58: 'Não autorizado. Entre em Contato com o emissor do cartão.',
                    59: 'Nome do Titular: Formato Inválido',
                    64: 'Transação não processada. Tente novamente.',
                    69: 'Transação não permitida para este produto ou serviço.',
                    70: 'Valor: Verifique o tamanho do texto.',
                    71: 'Valor: Formato Inválido',
                    72: 'Entre em Contato com o emissor do cartão.',
                    73: 'Valor: É obrigatório.',
                    74: 'Erro na Comunicação. Tente novamente.',
                    79: 'Cartão Expirado. A transação não pode ser reenviada. Entre em Contato com o emissor do cartão.',
                    80: 'Não autorizado. Entre em Contato com o emissor do cartão. (Saldo Insuficiente)',
                    82: 'Não autorizado transação para cartão de débito.',
                    83: 'Não autorizado. Entre em Contato com o emissor do cartão.',
                    84: 'Não autorizado. A transação não pode ser reenviada. Entre em Contato com o emissor do cartão.',
                    86: 'Cartão Expirado.',
                    88: 'Cliente não autorizado. Regularize seu site e entre em contato com a Rede para voltar a fazer transações..',
                    101: 'Não autorizado. Problemas no cartão, entre em contato com o emissor.',
                    102: 'Não autorizado. Verifique a situação da loja com o emissor.',
                    103: 'Não autorizado. Tente novamente.',
                    104: 'Não autorizado. Tente novamente.',
                    105: 'Não autorizado. Cartão Restrito.',
                    106: 'Erro no processamento do emissor. Tente novamente.',
                    107: 'Não autorizado. Tente novamente..',
                    108: 'Não autorizado. Valor não permitido para este tipo de cartão.',
                    109: 'Não autorizado. Cartão inexistente.',
                    110: 'Não autorizado. Tipo de transação não permitido para este cartão.',
                    111: 'Não autorizado. Saldo Insuficiente.',
                    112: 'Não autorizado. Cartão Vencido.',
                    113: 'Não autorizado. Identificou risco moderado pelo emissor.',
                    114: 'Não autorizado. O cartão não pertence à rede de pagamento.',
                    115: 'Não autorizado. Excedeu o limite de transações permitidas no período.',
                    116: 'Não autorizado. Entre em contato com o emissor do cartão.',
                    117: 'Transação não encontrada.',
                    118: 'Não autorizado. Cartão Bloqueado.',
                    119: 'Não autorizado. Código de Segurança Inválido.',
                    121: 'Erro ao processar. Tente novamente..',
                    122: 'Transação enviada anteriormente.',
                    123: 'Não autorizado. Bearer solicitou o fim das recorrências no emissor.',
                    124: 'Não autorizado. Entre em Contato com a REDE',
                    150: 'Timeout. Tente Novamente.',
                    204: 'Titular do cartão não registrado no programa de autenticação do emissor.',
                    351: 'Forbidden',
                    353: 'Transação não encontrada',
                    354: 'Transação com prazo expirado para reembolso',
                    357: 'Soma dos Reembolsos Valor maior que a transação',
                    358: 'Soma dos reembolsos de valor maior que o valor processado disponível para reembolso.',
                    368: 'Sem sucesso. Tente novamente.',
                    370: 'Falha na Requisição. Entre em Contato com a REDE',
                    371: 'Transação não disponível para reembolso. Tente novamente em algumas horas'
                };

                if (!(index in messages)) {
                    return 'Entre em contato com o emissor do cartão.';
                }
                return messages[index];
            }
        });

    </script>

</body>


</html>