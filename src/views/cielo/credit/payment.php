<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Instituto Vida Renovada - Rede API</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="<?= $routeAssets . '/css/card.css' ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?= $routeAssets . '/css/sweetalert.css' ?>">
    <style>
        .loader {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 50%;
            z-index: 2;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: rgba(0, 0, 0, 0.4);
        }

        .loader::before {
            height: 1em;
            width: 1em;
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -0.5em;
            margin-top: -1em;
            content: '';
            animation: spin 1s ease-in-out infinite;
            background: url('img/loader.svg') center center;
            background-size: cover;
            line-height: 1;
            text-align: center;
            font-size: 2em;
            color: rgba(#000, 0.75);
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>


<body>

<div class="container" style="padding-top: 10px;">

    <div id="google_translate_element" style="position:fixed; top:0; z-index:999;"></div>

    <div class="row">
        <div class="col-sm-6 col-md-2 d-flex align-items-center">
            <div class="partner">
                <a href="#">
                    <img src="<?= $routeAssets . '/img/logo-cielo.png' ?>" style="height: 50px; object-fit: cover;">
                </a>
            </div>
        </div>

        <div class="col-md-8 offset-md-1 mt-3">
            <h3 style="padding-top: 10px;">Instituto Vida Renovada - CHECKOUT</h3>
        </div>

    </div>

    <hr>

    <div class="row">
        <form method="post" action="#">
            <div class="row">

                <div class="col-md-4 offset-md-4 text-center">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Valor da sua doação</label>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <select name="currency" class="input-group-text" id="inputGroupSelect01">
                                    <option value="BRL" selected>R$</option>
                                    <option value="USD">US$</option>
                                    <option value="EUR">€</option>
                                </select>
                            </div>
                            <input type="text" name="amount" class="form-control money" aria-label="Valor doação"
                                   placeholder="0,00" autofocus required
                            >
                        </div>

                    </div>

                </div>

            </div>


            <h3 class="mb-2">Informações do pagamento</h3>

            <div class="row">

                <div class="col-md-3 mb-4">
                    <div class="form-check form-check-inline" style="width: 50%;">
                        <input class="option-input card-type"
                               type="radio"
                               name="card_type"
                               id="inlineRadio2"
                               value="authorizeCredit"
                               required>
                        <label class="form-check-label" for="inlineRadio2">Crédito</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="option-input card-type"
                               type="radio"
                               name="card_type"
                               id="inlineRadio1"
                               value="authorizeDebit"
                               checked
                               required>
                        <label class="form-check-label" for="inlineRadio1">Débito</label>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6"
                     style="background-color: #f8f8f8; border: 1px solid #ccc; padding: 45px 30px 0 30px; border-radius: 10px; box-shadow: 0 0 10px #f4f4f2;">
                    <div class="row">

                        <div class="col-md-6">
                            <label for="cardNumber">Número do cartão</label>
                            <div class="input-group form-group">
                                <input type="text" name="cardNumber" class="form-control credit-card"
                                       id="cardNumber"
                                       required
                                >
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                      <i class="fa fa-credit-card"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="brand" value="Visa">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cardholderName">Nome impresso no cartão</label>
                                <input type="text"
                                       name="cardholderName"
                                       class="form-control"
                                       id="cardholderName"
                                       required
                                >
                            </div>

                        </div>

                        <div class="col-md-6">

                            <p style="display: block; margin-bottom: .5rem;">Válido até</p>
                            <!--                            <div class="d-flex">-->
                            <!--                                <label for="expirationDate">-->
                            <!--                                    <span class="payment-card__hint"></span>-->
                            <!--                                    <input type="text" name="expirationDate"-->
                            <!--                                           class="form-control expiration_input payment-card__field" id="expirationDate"-->
                            <!--                                           placeholder="MM/AA"-->
                            <!--                                           required value="12/20">-->
                            <!--                                </label>-->
                            <!---->
                            <!--                            </div>-->
                            <div class="d-flex">
                                <label for="expirationMonth">
                                    <span class="payment-card__hint"></span>
                                    <input type="text" name="expirationMonth"
                                           class="form-control month_input payment-card__field" id="expirationMonth"
                                           placeholder="MM"
                                           required
                                    >
                                </label>

                                <span class="payment-card__separator">/</span>

                                <label for="expirationYear">
                                    <span class="payment-card__hint"></span>
                                    <input type="text" name="expirationYear"
                                           class="form-control year_input payment-card__field" id="expirationYear"
                                           placeholder="YY"
                                           required
                                    >
                                </label>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="securityCode">Código de segurança</label>
                                <input type="text" name="securityCode" class="form-control ccv"
                                       id="securityCode"
                                       placeholder="CCV"
                                       data-minlength="3"
                                       required
                                >
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="cartaodaora"></div>
                </div>

            </div>
            <div class="col-md-4 text-center mt-4 mb-5">
                <button name="send-donation" class="btn btn-primary pull-right btn-payment">Seguir para pagamento
                </button>
            </div>

        </form>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="<?= $routeAssets . '/js/jquery.card.js' ?>"></script>

    <script src="<?= $routeAssets . '/js/cielo-script.js' ?>"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="<?= $routeAssets . '/js/sweetalert.min.js' ?>"></script>


    <script>
        $(document).ready(function () {
            // Retorno do Débito
            <?php if(isset($successDebit)): ?>

                if (<?= $successDebit ?>) {
                    successMessage()
                } else {
                    customErrorMessage('Não foi possível realizar a tranzação!')
                }

            <?php endif; ?>

            var valueExpiratioDate;

            $('.money').mask("##0,00", {
                reverse: true
            });

            $('.credit-card').mask("0000 0000 0000 0000");

            $('.ccv').mask("0000");

            $('.expiration_input').mask("00/00");

            $('.month_input').mask("00");

            $('.year_input').mask("00");

            $route = '<?= $routeAssets . '/cielo/' ?>' + $('input[name=card_type]:checked').val();
            $('.card-type').on('change', function () {
                $route = $(this).val();
            });

            $('.year_input').on('keyup', function (e) {

                valueExpiratioDate = $('.month_input').val() + '/' + this.value

                $('.expiration_input').val(valueExpiratioDate)
            });

            $('.btn-payment').on('click', function () {
                ;
                var $emptyFields = $('form input:required').filter(function () {
                    return $.trim(this.value) === "";
                });

                // Se não definir a Bandeira define 'Visa' como padrão
                if (card.cardType !== undefined && card.carType !== '' && card.carType !== null && $('input[name=brand]').val() !== 'Visa') {
                    var caps = card.cardType.charAt(0).toUpperCase() + card.cardType.slice(1);
                    $('input[name=brand]').val(caps);
                }

                if (!$emptyFields.length) {
                    var $this = $(this);
                    var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Processando...';
                    $this.attr('disabled', true);
                    if ($(this).html() !== loadingText) {
                        $this.data('original-text', $(this).html());
                        $this.html(loadingText);
                    }

                    try {
                        data = getInputToPost();

                        $.post($route, data, function (response) {
                            var payment = response.message.Payment

                            if ($('input[name=card_type]:checked').val() == 'authorizeDebit') {
                                window.location.href = payment.AuthenticationUrl;

                                return false;
                            }

                            if (payment.ReturnCode == 4 || payment.ReturnCode == 6) {
                                successMessage();
                            } else {
                                message = ' Code: ' + payment.ReturnCode + ' | Message: ' + payment.ReturnMessage
                                customErrorMessage(message);
                            }

                            $this.html($this.data('original-text'));
                        })
                            .fail(function (err) {
                                var response = err.responseJSON,
                                    errorMessage = '';

                                // console.log(response);
                                for (i in response.message) {
                                    errorMessage += 'Code: ' + response.message[i].Code + ' | Message: ' + response.message[i].Message + ", ";
                                }
                                // var message = translateRedeItauMessage(response.message.returnCode);

                                customErrorMessage(errorMessage);
                            })
                    } catch (err) {
                        console.log(err);
                        errorMessage();
                    }

                    return false;
                }

                // return false;

            });

            function getInputToPost() {

                var expirationMonth = $('input[name=expirationMonth]').val(),
                    expirationYear = $('input[name=expirationYear]').val(),
                    nowDate = new Date(),
                    year = nowDate.getFullYear().toString();

                expirationYear = year.substring(0, 2) + expirationYear;

                var expirationDate = expirationMonth + '/' + expirationYear;

                return {
                    "paymentAmount": $('input[name=amount]').val(),
                    "cardholderName": $('input[name=cardholderName]').val(),
                    "cardNumber": $('input[name=cardNumber]').val(),
                    "expirationDate": expirationDate,
                    "securityCode": $('input[name=securityCode]').val(),
                    "brand": $('input[name=brand]').val(),
                    "currency": $('select[name=currency]').find(":selected").val(),
                    "URL": window.location.href
                };
            }

            function errorMessage() {
                swal({
                        title: "Ocorreu um Erro!",
                        text: "Tente novamente ou entre em Contato com o Administrador do Sistema!",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Tentar Novamente!",
                        closeOnConfirm: true
                    },
                    function () {
                        location.reload();
                    });
            };

            function successMessage() {
                swal({
                        title: "Obrigado!",
                        text: "Recebemos sua Contribuição!",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "OK!",
                        closeOnConfirm: true
                    },
                    function () {
                        $.get(window.location.href, function( data ) {});
                    });
            };

            function customErrorMessage(text) {
                swal({
                        title: "Ocorreu um Erro!",
                        text: text,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Tentar Novamente!",
                        closeOnConfirm: true
                    },
                    function () {
                        $.get(window.location.href, function( data ) {});
                    });
            };

            function translateCieloMessage(index) {
                var messages = {
                    4: 'Operação realizada com sucesso.',
                    6: 'Operação realizada com sucesso.',
                    5: 'Operação Não Autorizada.',
                    57: 'Cartão Expirado.'
                };

                if (!(index in messages)) {
                    return 'Entre em contato com o emissor do cartão.';
                }
                return messages[index];
            }
        });

    </script>

</body>


</html>