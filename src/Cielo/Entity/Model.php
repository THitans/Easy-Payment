<?php
/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 30/07/18
 * Time: 00:03
 */

namespace Cielo\Entity;

trait Model
{
    public function fill(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            if ($key == 'paymentAmount') {
                $this->$key = preg_replace("/\D+/", "", $value);
            } elseif ($key == 'cardNumber') {
                $this->$key = preg_replace("/\D+/", "", $value);
            }  elseif ($key == 'expirationDate') {
                $this->$key = str_replace(" ", "", $value);
            } else {
                $this->$key = $value;
            }
        }

        return $this;
    }
}