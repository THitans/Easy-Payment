<?php
/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:31
 */

namespace Cielo\Entity;

/**
 * Class CreditAuthorization
 * Docs para atributos https://www.userede.com.br/desenvolvedores/pt/produto/e-Rede#documentacao
 * @package Cielo\Entity
 */
class BaseAuthorization
{
    use Model;

    /**
     * Numero de identificação do Pedido.
     *
     * @required true
     * @size 50
     * @var string
     */
    public $merchantOrderId;

    /**
     * Nome do Comprador.
     *
     * @required false
     * @size 255
     * @var string
     */
    public $customerName;

    /**
     * Tipo do Meio de Pagamento.
     *
     * @required true
     * @size 100
     * @var string
     */
    public $paymentType;

    /**
     * Valor total da transação sem separador de milhar e decimal.
     * Exemplos: R$ 10,00 = 1000 | R$ 0,50 = 50
     *
     * @required true
     * @size 15
     * @var integer
     */
    public $paymentAmount;

    /**
     * Moeda na qual o pagamento será feito (BRL, USD, EUR).
     *
     * @required false
     * @size 3
     * @var string
     */
    public $currency;

    /**
     * Número de Parcelas.
     *
     * @required true
     * @size 2
     * @var integer
     */
    public $paymentInstallments;

    /**
     * Texto impresso na fatura bancaria comprador - Exclusivo para VISA/MASTER - não permite caracteres especiais.
     *
     * @required false
     * @size 13
     * @var string
     */
    public $paymentSoftDescriptor;

    /**
     * Número do Cartão do Comprador.
     *
     * @required true
     * @size 19
     * @var string
     */
    public $cardNumber;

    /**
     * Nome do portador impresso no cartão.
     *
     * @required true
     * @size 25
     * @var string
     */
    public $cardholderName;

    /**
     * Data de validade impresso no cartão.
     * Ex.: 10/2030
     *
     * @required true
     * @size 7
     * @var string
     */
    public $expirationDate;

    /**
     * Código de segurança impresso no verso do cartão - Ver Anexo.
     *
     * @required false
     * @size 4
     * @var string
     */
    public $securityCode;

    /**
     * Bandeira do cartão (Visa / Master / Amex / Elo / Aura / JCB / Diners / Discover / Hipercard).
     *
     * @required true
     * @size 10
     * @var string
     */
    public $brand;
}