<?php
/**
 * Created by PhpStorm.
 * User: horecio
 * Date: 09/09/18
 * Time: 23:43
 */

namespace Cielo\Entity;


interface BodyInterface
{
    /**
     * Formata o Body para transação
     * https://developercielo.github.io/manual/cielo-ecommerce
     *
     * @return array
     */
    public function getDataBodyRequest();
}