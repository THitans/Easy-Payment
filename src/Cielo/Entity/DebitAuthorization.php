<?php
/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:31
 */

namespace Cielo\Entity;

/**
 * Class CreditAuthorization
 * Docs para atributos https://www.userede.com.br/desenvolvedores/pt/produto/e-Rede#documentacao
 * @package Cielo\Entity
 */
class DebitAuthorization extends BaseAuthorization implements BodyInterface
{
    use Model;

    /**
     * Nome do Comprador.
     *
     * @required false
     * @size 255
     * @var string
     */
    public $customerName = 'Comprador Cartão de débito';

    /**
     * Tipo do Meio de Pagamento.
     *
     * @required true
     * @size 100
     * @var string
     */
    public $paymentType = 'DebitCard';

    /**
     * Tipo do Meio de Pagamento.
     *
     * O Cartão de débito por padrão exige que o portador seja direcionado para o ambiente Bancário, onde será avaliada
     * a senha e dados informados pela loja. Existe a opção de não autenticar transações de débito, porem é necessario
     * que o banco emissor do cartão permita tal transação Essa não é uma permissão concedida pela cielo, o lojista deve
     * acionar o banco e solicitar a permissão
     *
     * @required true
     * @var boolean
     */
    public $paymentAuthenticate = true;

    /**
     * Tipo do Meio de Pagamento.
     *
     * @required true
     * @size 100
     * @var string
     */
    public $paymentReturnUrl = 'http://0.0.0.0:8090/public/cielo/payment';

    /**
     * @inheritdoc
     */
    public function getDataBodyRequest()
    {
        return [
            'MerchantOrderId' => $this->merchantOrderId,
            'Customer' => [
               'Name' => $this->customerName
            ],
            'Payment' => [
                'Type' => $this->paymentType,
                'Authenticate' => $this->paymentAuthenticate,
                'Amount' => $this->paymentAmount,
                'ReturnUrl' => $this->paymentReturnUrl,
                'SoftDescriptor' => $this->paymentSoftDescriptor,
                'DebitCard' => [
                    'CardNumber' => $this->cardNumber,
                    'Holder' => $this->cardholderName,
                    'ExpirationDate' => $this->expirationDate,
                    'SecurityCode' => $this->securityCode,
                    'Brand' => $this->brand,
                ],
            ]
        ];
    }
}