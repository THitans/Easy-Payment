<?php

/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:05
 */

namespace Cielo\Controllers;

use Cielo\Entity\CreditAuthorization;
use Cielo\Entity\DebitAuthorization;
use GuzzleHttp\Exception\GuzzleException;

class PaymentController
{
    use AuthorizeTrait;

    // https://lipis.github.io/bootstrap-sweetalert/
    public function payment()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $successDebit = isset($_POST['PaymentId']) ? true : false;
        }

        $routeAssets = config_cielo('route_base');

        include view_path('cielo/credit/payment.php');
    }

    public function authorizeCredit()
    {
        try {
            $randNumber = time() . rand(1, 1000);

            $authorizationEntity = new CreditAuthorization();
            $authorizationEntity->fill($_POST);
            $authorizationEntity->merchantOrderId = 'P' . $randNumber;
            $authorizationEntity->paymentInstallments = 1;
            $authorizationEntity->paymentSoftDescriptor = 'Inst.Vid.Ren';

            $response = $this->authorize($authorizationEntity->getDataBodyRequest());

            echo json_response(200, \GuzzleHttp\json_decode($response->getBody()));

        } catch (GuzzleException $guzzleException) {
            echo json_response(500, \GuzzleHttp\json_decode($guzzleException->getResponse()->getBody(true)));
        } catch (\Exception $exception) {
            echo json_response(500, $exception->getMessage());
        }
    }

    public function authorizeDebit()
    {
        try {
            $randNumber = time() . rand(1, 1000);

            $authorizationEntity = new DebitAuthorization();
            $authorizationEntity->fill($_POST);
            $authorizationEntity->merchantOrderId = 'P' . $randNumber;
            $authorizationEntity->paymentInstallments = 1;
            $authorizationEntity->paymentSoftDescriptor = 'Inst.Vid.Ren';

            $response = $this->authorize($authorizationEntity->getDataBodyRequest());

            echo json_response(200, \GuzzleHttp\json_decode($response->getBody()));

        } catch (GuzzleException $guzzleException) {
            echo json_response(500, \GuzzleHttp\json_decode($guzzleException->getResponse()->getBody(true)));
        } catch (\Exception $exception) {
            echo json_response(500, $exception->getMessage());
        }
    }
}