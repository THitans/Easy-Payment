<?php

/**
 * Created by PhpStorm.
 * User: THitans
 * Date: 29/07/18
 * Time: 11:06
 */
namespace Cielo\Controllers;

use GuzzleHttp\Client;

trait AuthorizeTrait
{
    private function authorize($body)
    {
            return $this->client()->request('POST', config_cielo('erede_url') . '/1/sales/', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Accept-Encoding' => 'gzip',
                    'MerchantId' => config_cielo('merchant_id'),
                    'MerchantKey' => config_cielo('merchant_key'),
                    'RequestId' => uniqid()
                ],
                'json' => $body
            ]);
    }

    /**
     * @return \GuzzleHttp\Client
     */
    private function client()
    {
        return new Client();
    }
}