<?php
/**
 * https://stackoverflow.com/questions/38686776/how-do-i-use-fastroute
 */
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
//    $r->addRoute('GET', '/assets', \ERede\Controllers\CreditController::class . '/assets');

    $r->addRoute('POST', '/public/erede/authorizeCredit', \ERede\Controllers\CreditController::class . '/authorizeCredit');

    $r->addRoute('POST', '/public/erede/authorizeDebit', \ERede\Controllers\CreditController::class . '/authorizeDebit');

    $r->addRoute('GET', '/public/erede/payment', \ERede\Controllers\CreditController::class . '/payment');

    $r->addRoute('POST', '/public/cielo/authorizeCredit', \Cielo\Controllers\PaymentController::class . '/authorizeCredit');

    $r->addRoute('POST', '/public/cielo/authorizeDebit', \Cielo\Controllers\PaymentController::class . '/authorizeDebit');

    $r->addRoute('GET', '/public/cielo/payment', \Cielo\Controllers\PaymentController::class . '/payment');
    $r->addRoute('POST', '/public/cielo/payment', \Cielo\Controllers\PaymentController::class . '/payment');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo json_response(404, 'Página não encontrada.');
        die;

        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo json_response(405, 'Método Http não permitido para esta rota.');
        die;

        break;

    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        list($class, $method) = explode("/", $handler, 2);
        call_user_func_array(array(new $class, $method), $vars);

        break;
}

